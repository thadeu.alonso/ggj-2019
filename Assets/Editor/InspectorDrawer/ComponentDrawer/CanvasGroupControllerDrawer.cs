﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CanvasGroupController), true)]
public class CanvasGroupControllerDrawer : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Toggle", GUILayout.Width(50), GUILayout.Height(25)))
            Selection.activeGameObject.SendMessage("Toogle", SendMessageOptions.RequireReceiver);
    }
}
#endif
