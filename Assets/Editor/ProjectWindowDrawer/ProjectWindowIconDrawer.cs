﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections.Generic;
using System;

public class ProjectWindowIconDrawer
{
    static string iconFolderPath = "Assets/Sprites/ProjectWindowIcon/Icons/";
    static string colorFolderPath = "Assets/Sprites/ProjectWindowIcon/Colors/";
    static string projectWindowBGColorPath = "Assets/Sprites/ProjectWindowIcon/Colors/ProjectWindowBackgroundColor.png";
    static Dictionary<Type, string> iconPathForType = new Dictionary<Type, string>()
    {
        { typeof(IntSO), iconFolderPath + "IntSOIcon.png" },
        { typeof(FloatSO), iconFolderPath + "FloatSOIcon.png" },
        { typeof(BoolSO), iconFolderPath + "BoolSOIcon.png" },
        { typeof(StringSO), iconFolderPath + "StringSOIcon.png" }, 
        { typeof(TransformSO), iconFolderPath + "TransformSOIcon.png" },
        { typeof(IntEventSO), iconFolderPath + "IntSOIcon.png" },
        { typeof(FloatEventSO), iconFolderPath + "FloatSOIcon.png" },
        { typeof(BoolEventSO), iconFolderPath + "BoolSOIcon.png" },
        { typeof(StringEventSO), iconFolderPath + "StringSOIcon.png" },
        { typeof(TransformEventSO), iconFolderPath + "TransformSOIcon.png" },
        { typeof(ManagerSO), iconFolderPath + "ManagerIcon.png" },
        { typeof(AudioSO), iconFolderPath + "AudioIcon.png" },
    };
    static Dictionary<Type, string> iconColorPathForType = new Dictionary<Type, string>()
    {
        { typeof(IConstant), colorFolderPath + "ConstantSOColor.png" },
        { typeof(IVariable), colorFolderPath + "VariableSOColor.png"},
        { typeof(AudioSO), colorFolderPath + "AudioSOColor.png" },
        { typeof(ManagerSO), colorFolderPath + "ManagerSOColor.png" },
        { typeof(EventSO), colorFolderPath + "EventSOColor.png" }, 
    };

    [DidReloadScripts]
    static ProjectWindowIconDrawer()
    {
        EditorApplication.projectWindowItemOnGUI = ItemOnGUI;
    }

    static void ItemOnGUI(string guid, Rect rect)
    {
        string assetPath = AssetDatabase.GUIDToAssetPath(guid);
        foreach (var item in iconColorPathForType)
        {
            UnityEngine.Object obj = AssetDatabase.LoadAssetAtPath(assetPath, item.Key);
            if (obj != null)
                DrawIcon(rect, item.Value);
        }
        foreach (var item in iconPathForType)
        {
            UnityEngine.Object obj = AssetDatabase.LoadAssetAtPath(assetPath, item.Key);
            if (obj != null)
                DrawIcon(rect, item.Value);
        }
    }

    static void DrawIcon(Rect rect, string iconPath)
    {
        rect.width = rect.height;

        //Texture2D projectWindowBGColorTex = (Texture2D)AssetDatabase.LoadAssetAtPath(projectWindowBGColorPath, typeof(Texture2D));
        Texture2D iconTex = (Texture2D)AssetDatabase.LoadAssetAtPath(iconPath, typeof(Texture2D));

        if(iconTex != null)
        {
            //GUI.DrawTexture(rect, projectWindowBGColorTex);
            GUI.DrawTexture(rect, iconTex);
        }
    }
}
#endif