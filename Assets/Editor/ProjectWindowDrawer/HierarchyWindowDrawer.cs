﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

public class HierarchyWindowDrawer
{
    static string trollIconGameObjectPosHolder;
    static bool foundHolder;
    static string nextGoNameAfterHolder;
    static int trollIconCurrentPos;
    static string trollIconPath = "Assets/Sprites/Editor/Hierarchy/Icons/Player.png";

    static Dictionary<string, string> iconPathForTag = new Dictionary<string, string>()
    {
        { "Player", "Assets/Sprites/Editor/Hierarchy/Icons/Player.png" },
    };

    [DidReloadScripts]
    static HierarchyWindowDrawer()
    {
        EditorApplication.hierarchyWindowItemOnGUI += ItemOnHierarchy;
        //EditorApplication.hierarchyWindowItemOnGUI += DrawTrollIconOnHierarchy;
    }

    static void ItemOnHierarchy(int instanceID, Rect rect)
    {
        GameObject go = EditorUtility.InstanceIDToObject(instanceID) as GameObject;
        if (go == null)
            return;
        foreach (var item in iconPathForTag)
        {
            if (go.CompareTag(item.Key))
            {
                DrawIconOnHierarchy(rect, item.Value);
                break;
            }
        }
    }

    static void DrawIconOnHierarchy(Rect rect, string iconPath)
    {
        Texture2D iconTex = (Texture2D)AssetDatabase.LoadAssetAtPath(iconPath, typeof(Texture2D));

        rect.x = rect.width + 10;
        rect.height = 22;
        rect.y -= 4;

        if (iconTex != null)
            GUI.Label(rect, iconTex);
    }

    static void DrawTrollIconOnHierarchy(int instanceID, Rect rect)
    {
        //GameObject go = EditorUtility.InstanceIDToObject(instanceID) as GameObject;
        //if (go == null)
        //    return;
        //if (trollIconGameObjectPosHolder == "")
        //{
        //    trollIconGameObjectPosHolder = go.name;
        //    foundHolder = true;
        //}
        //else
        //{
        //    if(foundHolder)
        //    {
        //        foundHolder = false;
        //        if(nextGoNameAfterHolder == "")
        //            nextGoNameAfterHolder = go.name;
        //        return;
        //    }
        //    else if (go.name == nextGoNameAfterHolder)
        //    {
        //        trollIconGameObjectPosHolder = nextGoNameAfterHolder;
        //        foundHolder = true;
        //        nextGoNameAfterHolder = "";
        //    }
        //    else
        //}

        //Texture2D iconTex = (Texture2D)AssetDatabase.LoadAssetAtPath(trollIconPath, typeof(Texture2D));

        //trollIconCurrentPos = ((trollIconCurrentPos + 1) % 800);
        //rect.y = trollIconCurrentPos - 100;
        //rect.x = rect.width + 10;
        //rect.width = 50;
        //rect.height = 50;

        //if (iconTex != null)
        //    GUI.Label(rect, iconTex);
    }
}
#endif
