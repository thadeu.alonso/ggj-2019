﻿using UnityEngine;

public class InstantiateController : MonoBehaviour
{
    [SerializeField] Transform instantiatePosition;
    [SerializeField] BoolReference instantiateRotationForward;

    public void InstantiatePrefab(GameObject _prefab)
    {
        Instantiate(_prefab, GetInstantiatePosition(), GetInstantiateRotation());
    }

    Vector3 GetInstantiatePosition()
    {
        if (instantiatePosition == null)
            return transform.position;
        else
            return instantiatePosition.position;
    }

    Quaternion GetInstantiateRotation()
    {
        if (instantiateRotationForward.Value == true)
            return transform.rotation;
        else
            return Quaternion.identity;
    }
}
