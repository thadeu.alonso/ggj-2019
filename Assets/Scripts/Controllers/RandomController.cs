﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomController : MonoBehaviour
{
    [SerializeField] Vector2Int intRange;
    [SerializeField] FloatReference floatMin;
    [SerializeField] FloatReference floatMax;

    [System.Serializable]
    public class Events
    {
        public UnityEventInt onRandomInt;
        public UnityEventFloat onRandomFloat;
    }
    [SerializeField] Events events;

    public void ChooseRandomInt()
    {
        int value = Random.Range(intRange.x, intRange.y);
        events.onRandomInt.Invoke(value);
    }
    public void ChooseRandomFloat()
    {
        float value = Random.Range(floatMin.Value, floatMax.Value);
        events.onRandomFloat.Invoke(value);
    }
}
