﻿using UnityEngine; 

public class DebugController : MonoBehaviour
{
    [SerializeField] bool debugRed; public bool DebugRed { get { return debugRed; } set { debugRed = value; } }

    public void Log(string _txt)
    {
        if (debugRed)                  
            _txt =AddRed(_txt);
    }
    public void Log(StringSO _txtSO)
    {
        if (_txtSO != null)
            Log(_txtSO.Value);
    }
    public void Log(StringComponent _txtComponent)
    {
        if (_txtComponent != null)
            Log(_txtComponent.Value);
    }

    public void Log(float _value)
    {
        Log(_value.ToString());
    }
    public void Log(FloatSO _floatSO)
    {
        if (_floatSO != null)
            Log(_floatSO.Value);
    }
    public void Log(FloatComponent _floatComponent)
    {
        if (_floatComponent != null)  
            Log(_floatComponent.Value);
    }

    public void Log(int _value)
    {
        Log(_value.ToString());
    }
    public void Log(IntSO _intSO)
    {
        if (_intSO != null)
            Log(_intSO.Value);
    }
    public void Log(IntComponent _intComponent)
    {
        if (_intComponent != null)
            Log(_intComponent.Value);
    }

    public void Break()
    {
        Debug.Break();
    }

    public static string AddRed(string text)
    {
        return "<color=red>" + text + "</color>";
    }
}

