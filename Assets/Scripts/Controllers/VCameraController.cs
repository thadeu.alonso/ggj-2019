﻿using UnityEngine;
using Cinemachine;

[RequireComponent(typeof(CinemachineVirtualCamera))]
public class VCameraController : MonoBehaviour
{
    [SerializeField] FloatReference rotationSpeed;

    CinemachineVirtualCamera camera;
    CinemachineTransposer transposer;

    private void Awake()
    {
        camera = GetComponent<CinemachineVirtualCamera>();
        transposer = camera.GetCinemachineComponent<CinemachineTransposer>();
    }

    public void Rotate(float _speed)
    {
        transposer.m_FollowOffset = Quaternion.Euler(Vector3.up * _speed) * transposer.m_FollowOffset;
    }

    public void Rotate(Vector2 _inputDirection)
    {
        transposer.m_FollowOffset = Quaternion.Euler(Vector3.up * rotationSpeed.Value * _inputDirection.x) * transposer.m_FollowOffset;
    }
}
