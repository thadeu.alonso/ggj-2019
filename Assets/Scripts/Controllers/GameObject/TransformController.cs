﻿using UnityEngine;

public class TransformController : MonoBehaviour
{
    [SerializeField] FloatReference speed;

	public void SetScale(float _value)
    {
        transform.localScale = Vector3.one * _value;
    }

    public void AddYScale(float _value)
    {
        transform.localScale += Vector3.up * _value;
    }

    public void MoveForward()
    {
        transform.Translate(Vector3.forward * speed.Value * Time.deltaTime, Space.Self);
    }
}
