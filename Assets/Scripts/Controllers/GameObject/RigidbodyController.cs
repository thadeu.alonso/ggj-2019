﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class RigidbodyController : MonoBehaviour
{
    [SerializeField] FloatReference acceleration;
    [SerializeField] FloatReference jumpForce;
    [SerializeField] FloatReference xZDrag;
    [SerializeField] BoolReference lookForwardWhenMove;
    [SerializeField] FloatReference speedLookForwardRotation;
    [SerializeField] BoolReference moveRelativeToCamera;
    [SerializeField] GameObject camera;
    public bool IsGrounded { get; set; }
    Rigidbody rigidbody;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    public void ApplyXZDrag()
    {
        Vector3 velocity = transform.InverseTransformDirection(rigidbody.velocity);
        float force_x = -1 * xZDrag.Value * velocity.x;
        float force_z = -1 * xZDrag.Value * velocity.z;
        rigidbody.AddRelativeForce(new Vector3(force_x, 0, force_z));
    }

    public void MoveForward()
    {
        if(moveRelativeToCamera.Value == true && camera != null)
            rigidbody.AddForce(GetForwardRelativeToCamera() * acceleration.Value * Time.deltaTime, ForceMode.VelocityChange);
        else
            rigidbody.AddForce(transform.forward * acceleration.Value * Time.deltaTime, ForceMode.VelocityChange);
    }
    public void MoveBackward()
    {
        if (moveRelativeToCamera.Value == true && camera != null)
            rigidbody.AddForce(-GetForwardRelativeToCamera() * acceleration.Value * Time.deltaTime, ForceMode.VelocityChange);
        else
            rigidbody.AddForce(-transform.forward * acceleration.Value * Time.deltaTime, ForceMode.VelocityChange);
    }
    public void MoveRight()
    {
        if (moveRelativeToCamera.Value == true && camera != null)
            rigidbody.AddForce(GetRightRelativeToCamera() * acceleration.Value * Time.deltaTime, ForceMode.VelocityChange);
        else
            rigidbody.AddForce(transform.right * acceleration.Value * Time.deltaTime, ForceMode.VelocityChange);
    }
    public void MoveLeft()
    {
        if (moveRelativeToCamera.Value == true && camera != null)
            rigidbody.AddForce(-GetRightRelativeToCamera() * acceleration.Value * Time.deltaTime, ForceMode.VelocityChange);
        else
            rigidbody.AddForce(-transform.right * acceleration.Value * Time.deltaTime, ForceMode.VelocityChange);
    }

    Vector3 GetForwardRelativeToCamera()
    {
        if (camera != null)
            return Vector3.ProjectOnPlane(camera.transform.forward, Vector3.up).normalized;

        return Vector3.zero;
    }

    Vector3 GetRightRelativeToCamera()
    {
        if (camera != null)
            return Quaternion.Euler(0, 90, 0) * GetForwardRelativeToCamera();

        return Vector3.zero;
    }

    Vector3 GetForwardRelativeToCamera(Vector2 _input)
    {
        if(camera != null)
        {
            Vector3 cameraForward = GetForwardRelativeToCamera();
            Vector3 cameraRight = Quaternion.Euler(0, 90, 0) * cameraForward;
            return (_input.y * cameraForward + _input.x * cameraRight).normalized;
        }

        return Vector3.zero;
    }

    public void Move(Vector2 _direction)
    {
        if (_direction == Vector2.zero)
            return;

        Vector3 moveDirection = new Vector3(_direction.x, 0, _direction.y);

        if (moveRelativeToCamera.Value == true && camera != null)
            moveDirection = GetForwardRelativeToCamera(_direction);

        rigidbody.AddForce(moveDirection * acceleration.Value * Time.deltaTime, ForceMode.VelocityChange);

        if (lookForwardWhenMove.Value == true)
            LookForwardSmooth(moveDirection);
    }

    void LookForwardSmooth(Vector3 _direction)
    {
        transform.rotation = Quaternion.Slerp(transform.rotation,
                             Quaternion.LookRotation(_direction),
                             Time.deltaTime * speedLookForwardRotation.Value);
    }

    public void TryJump()
    {
        if (IsGrounded)
            Jump();
    }
    void Jump()
    {
        rigidbody.AddForce(Vector3.up * jumpForce.Value, ForceMode.Impulse);
    }
}
