﻿using System.Collections;
using UnityEngine;

public class GameObjectController : MonoBehaviour
{
    [SerializeField] FloatReference selfDestructDelay;

    public void StartEnableAfterTime(float _delay)
    {
        StartCoroutine(EnableAfterTime(_delay)); 
    }
    IEnumerator EnableAfterTime(float _delay)
    {
        yield return new WaitForSeconds(_delay);
        gameObject.SetActive(true);
    }

    public void StartDisableAfterTime(float _delay)
    {
        if(isActiveAndEnabled)
            StartCoroutine(DisableAfterTime(_delay));
    }
    IEnumerator DisableAfterTime(float _delay)
    {
        yield return new WaitForSeconds(_delay);
        gameObject.SetActive(false);
    }

    public void Destroy()
    {
        Destroy(this.gameObject);
    }
    public void DestroyAfterTime(float _time)
    {
        Destroy(this.gameObject, _time);
    }
    public void DestroyAfterTime()
    {
        Destroy(this.gameObject, selfDestructDelay.Value);
    }
    public void DestroyAfterTime(FloatSO _floatSO)
    {
        Destroy(this.gameObject, _floatSO.Value);
    }
    public void DestroyAfterTime(FloatComponent _floatComponent)
    {
        Destroy(this.gameObject, _floatComponent.Value);
    }

    public void SetDontDestroyOnLoad()
    {
        DontDestroyOnLoad(gameObject);
    }
}
