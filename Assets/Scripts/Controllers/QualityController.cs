﻿using UnityEngine;
using UnityEngine.Events;

public class QualityController : MonoBehaviour
{
    [System.Serializable]
    public class Events
    {
        public UnityEventStringArray onReadQuality;
    }
    [SerializeField] Events events;

    public void ReadQuality()
    {
        events.onReadQuality.Invoke(QualitySettings.names);
    }

    public void ApplyQuality(int _value)
    {
        if (_value < 0 || QualitySettings.names.Length <= _value)
            return;
        QualitySettings.SetQualityLevel(_value);
    }

    public void ApplyQuality(IntSO _intSO)
    {
        if (_intSO == null || _intSO.Value < 0)
            return;

        QualitySettings.SetQualityLevel(_intSO.Value);
    }
}
[System.Serializable]
public class UnityEventStringArray : UnityEvent<string[]> { }
