﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ImageController : MonoBehaviour
{
    Image image;

    private void Awake()
    {
        image = GetComponent<Image>();
    }

    public void StartIncreasingAlphaDuringTime(float _time)
    {
        if(isActiveAndEnabled)
            StartCoroutine(IncreaseAlphaDuringTime(_time));
    }

    IEnumerator IncreaseAlphaDuringTime(float _time)
    {
        float startTime = _time;

        _time = 0;

        while (_time < startTime)
        {
            _time = Mathf.Min(_time + Time.deltaTime, startTime);
            image.color = new Color(image.color.r, image.color.g, image.color.b, _time/startTime);
            yield return null;
        }
    }
}
