﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Dropdown))]
public class DropdownController : MonoBehaviour
{
    [SerializeField] Dropdown dropdown;

    private void Awake()
    {
        GetReference();
    }

    public void FillDropdown(string[] _values)
    {
        if (dropdown == null)
            GetReference();

        dropdown.options.Clear();
        for (int i = 0; i < _values.Length; i++)
            dropdown.options.Add(new Dropdown.OptionData() { text = _values[i] });
    }
    public void SetValue(int _value)
    {
        if (dropdown == null)
            GetReference();

        if (dropdown.options.Count <= _value)
            return;

        dropdown.value = _value;
    }

    public void SetValue(IntSO _intSO)
    {
        if (_intSO == null || _intSO.Value < 0)
            return;
        if (dropdown == null)
            GetReference();

        if (dropdown.options.Count <= _intSO.Value)
            return;

        dropdown.value = _intSO.Value;
    }

    void GetReference()
    {
        dropdown = GetComponent<Dropdown>();
    }
}
