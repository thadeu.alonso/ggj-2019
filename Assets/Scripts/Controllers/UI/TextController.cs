﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TextController : MonoBehaviour
{
    [SerializeField] bool percent;
    Text text;

    public void SetValue(string _text)
    {
        if (text == null)
            text = GetComponent<Text>();
        text.text = _text;
    }

    public void SetValue(float _value)
    {
        if (percent)
            SetValue(((int)(_value * 100)).ToString() + "%");
        else
            SetValue(_value.ToString());
    }
}
