﻿using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class TMPController : MonoBehaviour
{
    [SerializeField] bool percent;
    TextMeshProUGUI text;

    public void SetValue(string _text)
    {
        if (text == null)
            text = GetComponent<TextMeshProUGUI>();
        text.text = _text;
    }

    public void SetValue(float _value)
    {
        if (percent)
            SetValue(((int)(_value * 100)).ToString() + "%");
        else
            SetValue(_value.ToString());
    }
}
