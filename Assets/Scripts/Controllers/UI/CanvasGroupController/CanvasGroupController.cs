﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(CanvasGroup))]
public class CanvasGroupController : MonoBehaviour
{
    CanvasGroup canvasGroup;
    Coroutine disableAfterTime;
    Coroutine enableAfterTime;  

    [System.Serializable]
    public class Events
    {
        public UnityEvent onEnable; 
        public UnityEvent onDisable;
        public UnityEvent onStartAppearing;
        public UnityEvent onStartDisappearing;
    }
    [SerializeField] Events events;

    public void Enable()
    {
        if (canvasGroup == null)
            GetReference();

        canvasGroup.alpha = 1;
        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;

        events.onEnable.Invoke();
    }
    public void EnableAfterTime(float time)
    {
        if (enableAfterTime != null)
            StopCoroutine(enableAfterTime);
        enableAfterTime = IEnumeratorHelper.DoAfterTime(this, Enable, time);
    }
    public void EnableAfterUnscaledTime(float time)
    {
        if (enableAfterTime != null)
            StopCoroutine(enableAfterTime);
        enableAfterTime = IEnumeratorHelper.DoAfterUnscaledTime(this, Enable, time);
    }

    public void StartSoftEnable()
    {
        if (canvasGroup == null)
            GetReference();

        StartCoroutine(SoftEnable());
    }
    IEnumerator SoftEnable()
    {
        float time = 0;
        while (time < 0.2f)
        {
            yield return null;
            time += Time.unscaledDeltaTime;
            canvasGroup.alpha = Mathf.Min(time, 0.2f) * 5;
        }
        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;
        events.onEnable.Invoke();
    }
    public void StartSoftEnableAfterTime(float time)
    {
        if (enableAfterTime != null)
            StopCoroutine(enableAfterTime);
        enableAfterTime = IEnumeratorHelper.DoAfterTime(this, StartSoftEnable, time);
    }
    public void StartSoftEnableAfterUnscaledTime(float time)
    {
        if (enableAfterTime != null)
            StopCoroutine(enableAfterTime);
        enableAfterTime = IEnumeratorHelper.DoAfterUnscaledTime(this, StartSoftEnable, time);
    }

    public void Disable()
    {
        if (canvasGroup == null)
            GetReference();

        canvasGroup.alpha = 0;
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;

        events.onDisable.Invoke();
    }
    public void DisableAfterTime(float time)
    {
        if (disableAfterTime != null)
            StopCoroutine(disableAfterTime);
        disableAfterTime = IEnumeratorHelper.DoAfterTime(this, Disable, time);
    }
    public void DisableAfterUnscaledTime(float time)
    {
        if (disableAfterTime != null)
            StopCoroutine(disableAfterTime);
        disableAfterTime = IEnumeratorHelper.DoAfterUnscaledTime(this, Disable, time);
    }

    public void StartSoftDisable()
    {
        if (canvasGroup == null)
            GetReference();

        StartCoroutine(SoftDisable());
    }
    IEnumerator SoftDisable()
    {
        float totalTime = 0.2f;
        float time = totalTime;
        while (time > 0)
        {
            yield return null;
            time -= Time.unscaledDeltaTime;
            canvasGroup.alpha = Mathf.Max(time * 5, 0);
        }
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
        events.onDisable.Invoke();
    }
    public void StartSoftDisableAfterTime(float time)
    {
        if (disableAfterTime != null)
            StopCoroutine(disableAfterTime);
        disableAfterTime = IEnumeratorHelper.DoAfterTime(this, StartSoftDisable, time);
    }
    public void StartSoftDisableAfterUnscaledTime(float time)
    {
        if (disableAfterTime != null)
            StopCoroutine(disableAfterTime);
        disableAfterTime = IEnumeratorHelper.DoAfterUnscaledTime(this, StartSoftDisable, time);
    }

    public void StartAppearing(float duration)
    {
        StartCoroutine(Appear(duration));
    }
    IEnumerator Appear(float duration)
    {
        if (canvasGroup == null)
            GetReference();
        events.onStartAppearing.Invoke();
        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;
        float time = 0;
        while (time < duration)
        {
            yield return null;
            time += Time.unscaledDeltaTime;
            canvasGroup.alpha = Mathf.Min(time / duration, duration);
        }
        Enable();
    }
    public void StartDisappearing(float duration)
    {
        StartCoroutine(Disappear(duration));
    }
    IEnumerator Disappear(float duration)
    {
        if (canvasGroup == null)
            GetReference();
        events.onStartAppearing.Invoke();
        float time = 0;
        while (time < duration)
        {
            yield return null;
            time += Time.unscaledDeltaTime;
            canvasGroup.alpha = Mathf.Max(1.0f - time / duration, 0);
        }
        Disable();
    }

    public void Toogle()
    {
        if (canvasGroup == null)
            GetReference();

        if (!canvasGroup.interactable)
            Enable();
        else
            Disable();
    }
    public void ToogleAfterTime(float time)
    {
        IEnumeratorHelper.DoAfterTime(this, Toogle, time);
    }

    private void GetReference()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }
}
