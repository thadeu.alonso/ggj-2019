﻿using UnityEngine;

public class RaycastController : MonoBehaviour
{
    [SerializeField] BoolReference onUpdate;
    [SerializeField] BoolReference ignoreTriggers;
    [SerializeField] Vector3 direction;

    [System.Serializable]
    public class Events
    {
        public UnityEventFloat distance;
    }
    [SerializeField] Events events;

    private void Update()
    {
        if (onUpdate.Value)
            TestHit();
    }

    void TestHit()
    {
        RaycastHit hit;
        QueryTriggerInteraction qi = QueryTriggerInteraction.UseGlobal;
        if (ignoreTriggers.Value)
            qi = QueryTriggerInteraction.Ignore;
        Physics.Raycast(transform.position, direction.normalized, out hit, direction.magnitude, LayerMask.NameToLayer("'"), qi);
        if (hit.collider != null)
            events.distance.Invoke(hit.distance);
    }
}
