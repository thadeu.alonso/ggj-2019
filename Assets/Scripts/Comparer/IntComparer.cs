﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class IntComparer : MonoBehaviour
{
    [SerializeField] bool onStart;
    [System.Serializable]
    public class ValueCheckEvent
    {
        public IntReference varTest;
        public ENumberCompareMode eCompareMode;
        public IntReference compareValue;
        [System.Serializable]
        public class Response
        {
            public UnityEvent onTrue;
            public UnityEvent onFalse;
        }
        public Response response;
    }
    [SerializeField] List<ValueCheckEvent> fixedCompareEvents;

    [System.Serializable]
    public class DynamicValueCheckEvent
    {
        public ENumberCompareMode eCompareMode;
        public IntReference compareValue;
        [System.Serializable]
        public class Response
        {
            public UnityEvent onTrue;
            public UnityEvent onFalse;
        }
        public Response response;
    }
    [SerializeField] List<DynamicValueCheckEvent> dynamicCompareEvents;

    private void Start()
    {
        if (onStart)
            Compare();
    }

    public void Compare()
    {
        foreach (ValueCheckEvent fixedCompareEvent in fixedCompareEvents)
        {
            switch (fixedCompareEvent.eCompareMode)
            {
                case ENumberCompareMode.Equals:
                    if (fixedCompareEvent.varTest.Value == fixedCompareEvent.compareValue.Value)
                        fixedCompareEvent.response.onTrue.Invoke();
                    else
                        fixedCompareEvent.response.onFalse.Invoke();
                    break;
                case ENumberCompareMode.GreaterThan:
                    if (fixedCompareEvent.varTest.Value > fixedCompareEvent.compareValue.Value)
                        fixedCompareEvent.response.onTrue.Invoke();
                    else
                        fixedCompareEvent.response.onFalse.Invoke();
                    break;
                case ENumberCompareMode.LessThan:
                    if (fixedCompareEvent.varTest.Value < fixedCompareEvent.compareValue.Value)
                        fixedCompareEvent.response.onTrue.Invoke();
                    else
                        fixedCompareEvent.response.onFalse.Invoke();
                    break;
                case ENumberCompareMode.LessThanOrEqual:
                    if (fixedCompareEvent.varTest.Value <= fixedCompareEvent.compareValue.Value)
                        fixedCompareEvent.response.onTrue.Invoke();
                    else
                        fixedCompareEvent.response.onFalse.Invoke();
                    break;
                case ENumberCompareMode.GreaterThanOrEqual:
                    if (fixedCompareEvent.varTest.Value >= fixedCompareEvent.compareValue.Value)
                        fixedCompareEvent.response.onTrue.Invoke();
                    else
                        fixedCompareEvent.response.onFalse.Invoke();
                    break;
            }
        }
    }

    void Compare(int _value)
    {
        foreach (DynamicValueCheckEvent dynamicCompareEvent in dynamicCompareEvents)
        {
            switch (dynamicCompareEvent.eCompareMode)
            {
                case ENumberCompareMode.Equals:
                    if (_value == dynamicCompareEvent.compareValue.Value)
                        dynamicCompareEvent.response.onTrue.Invoke();
                    else
                        dynamicCompareEvent.response.onFalse.Invoke();
                    break;
                case ENumberCompareMode.GreaterThan:
                    if (_value > dynamicCompareEvent.compareValue.Value)
                        dynamicCompareEvent.response.onTrue.Invoke();
                    else
                        dynamicCompareEvent.response.onFalse.Invoke();
                    break;
                case ENumberCompareMode.LessThan:
                    if (_value < dynamicCompareEvent.compareValue.Value)
                        dynamicCompareEvent.response.onTrue.Invoke();
                    else
                        dynamicCompareEvent.response.onFalse.Invoke();
                    break;
                case ENumberCompareMode.LessThanOrEqual:
                    if (_value <= dynamicCompareEvent.compareValue.Value)
                        dynamicCompareEvent.response.onTrue.Invoke();
                    else
                        dynamicCompareEvent.response.onFalse.Invoke();
                    break;
                case ENumberCompareMode.GreaterThanOrEqual:
                    if (_value >= dynamicCompareEvent.compareValue.Value)
                        dynamicCompareEvent.response.onTrue.Invoke();
                    else
                        dynamicCompareEvent.response.onFalse.Invoke();
                    break;
            }
        }
    }
}
