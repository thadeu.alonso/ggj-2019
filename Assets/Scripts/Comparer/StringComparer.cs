﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StringComparer : MonoBehaviour
{
    [SerializeField] bool onStart;
    [System.Serializable]
    public class ValueCheckEvent
    {
        public StringReference varTest;
        public StringReference compareValue;
        [System.Serializable]
        public class Response
        {
            public UnityEvent isEqual;
            public UnityEvent isDifferent;
        }
        public Response response;
    }
    [SerializeField] List<ValueCheckEvent> fixedCompareEvents;

    [System.Serializable]
    public class DynamicCompareEvent
    {
        public StringReference compareValue;
        [System.Serializable]
        public class Response
        {
            public UnityEvent isEqual;
            public UnityEvent isDifferent;
        }
        public Response response;
    }
    [SerializeField] List<DynamicCompareEvent> dynamicCompareEvents;

    private void Start()
    {
        if (onStart)
            Compare();
    }

    public void Compare()
    {
        foreach (var fixedCompareEvent in fixedCompareEvents)
        {
            if (fixedCompareEvent.varTest.Value == fixedCompareEvent.compareValue.Value)
                fixedCompareEvent.response.isEqual.Invoke();
            else
                fixedCompareEvent.response.isDifferent.Invoke();
        }
    }

    public void Compare(string _value)
    {
        foreach (var dynamicCompareEvent in dynamicCompareEvents)
        {
            if (_value == dynamicCompareEvent.compareValue.Value)
                dynamicCompareEvent.response.isEqual.Invoke();
            else
                dynamicCompareEvent.response.isDifferent.Invoke();
        }
    }
}
