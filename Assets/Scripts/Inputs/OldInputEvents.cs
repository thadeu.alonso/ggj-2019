﻿using UnityEngine;
using UnityEngine.Events;

public class OldInputEvents : MonoBehaviour
{
    [SerializeField] FloatReference shootCooldown;
    float currentShootCooldown;

    [System.Serializable]
    public class Events
    {
        public UnityEventVector2 onMove;
        public UnityEvent onJump;
        public UnityEvent onShoot;
        public UnityEvent onTooglePause;
    }
    [SerializeField] Events events;

    public bool Stop { get; set; }

    private void Update()
    {
        if (currentShootCooldown > 0)
            currentShootCooldown -= Time.deltaTime;

        if (Stop)
            return;

        ReadPause();

        if (Time.deltaTime == 0)
            return;

        ReadJump();
        ReadShoot();
    }

    private void FixedUpdate()
    {
        if (Stop || Time.deltaTime == 0)
            return;

        ReadMoveDirection();
    }

    void ReadMoveDirection()
    {
        if(Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
            events.onMove.Invoke(new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")));
    }

    void ReadJump()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            events.onJump.Invoke();
    }

    void ReadShoot()
    {
        if (Input.GetMouseButtonDown(0) && currentShootCooldown <= 0)
        {
            currentShootCooldown = shootCooldown.Value;
            events.onShoot.Invoke();
        }
    }

    void ReadPause()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            events.onTooglePause.Invoke();
    }
}
