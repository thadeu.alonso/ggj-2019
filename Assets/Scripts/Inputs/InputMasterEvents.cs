﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputMasterEvents : MonoBehaviour
{
    [SerializeField] InputMaster inputMaster;

    [System.Serializable]
    public class Events
    {
        public UnityEvent onPlayerShoot;
        public UnityEventVector2 onPlayerMove;
    }
    [SerializeField] Events events;

    private void Awake()
    {
        if (inputMaster == null)
            return;
        
        inputMaster.Player.Shoot.performed += _ => events.onPlayerShoot.Invoke();
        inputMaster.Player.Movement.performed += ctx => events.onPlayerMove.Invoke(ctx.ReadValue<Vector2>());
    }

    private void OnEnable()
    {
        if (inputMaster != null)
            inputMaster.Enable();
    }

    private void OnDisable()
    {
        if (inputMaster != null)
            inputMaster.Disable();
    }
}

[System.Serializable]
public class UnityEventVector2 : UnityEvent<Vector2> { }
