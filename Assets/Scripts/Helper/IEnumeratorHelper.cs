﻿using System;
using System.Collections;
using UnityEngine;

public static class IEnumeratorHelper
{
    public static Coroutine DoAfterTime(MonoBehaviour mb, Action action, float time){
        return mb.StartCoroutine(DoAfterTime(action, time));
    }
    public static Coroutine DoAfterUnscaledTime(MonoBehaviour mb, Action action, float time)    {
        return mb.StartCoroutine(DoAfterUnscaledTime(action, time)); 
    }

    static IEnumerator DoAfterTime(Action action, float time){
        yield return new WaitForSeconds(time);
        action.Invoke();
    }
    static IEnumerator DoAfterUnscaledTime(Action action, float time)
    {
        yield return new WaitForSecondsRealtime(time); 
        action.Invoke();
    }

    public static Coroutine DoRepatingAfterTime(MonoBehaviour mb, Action action, float time){
        return mb.StartCoroutine(DoRepatingAfterTime(action, time));
    }

    static IEnumerator DoRepatingAfterTime(Action action, float time){
        while(true)
        {
            yield return new WaitForSeconds(time);
            action.Invoke();
        }
    }
}