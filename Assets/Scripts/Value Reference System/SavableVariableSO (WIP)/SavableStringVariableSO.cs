﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewSavableStringVariable", menuName = "Values/Savable/String (WIP)")]
public class SavableStringVariableSO : StringVariableSO
{
    public void Load()
    {

    }

    public void Save()
    {

    }
}