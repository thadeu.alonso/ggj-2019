﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewSavableFloatVariable", menuName = "Values/Savable/Float (WIP)")]
public class SavableFloatVariableSO : FloatVariableSO
{
    public void Load()
    {

    }

    public void Save()
    {

    }
}