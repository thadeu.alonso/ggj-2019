﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewSavableIntVariable", menuName = "Values/Savable/Int (WIP)")]
public class SavableIntVariableSO : IntVariableSO
{
    public void Load()
    {

    }

    public void Save()
    {

    }
}