﻿using System.Collections.Generic;
using UnityEngine;

public enum ENumberCompareMode { Equals, GreaterThan, LessThan, LessThanOrEqual, GreaterThanOrEqual }

public class FloatComponent : MonoBehaviour
{
    public string name;
    [SerializeField] FloatReference value; public float Value { get { return this.value.Value; } set { if (this.value.Value == value) return; this.value.Value = value; OnChange(); } }
    [SerializeField] FloatReference defaultValue;
    
    [System.Serializable]
    public class Events
    {
        public UnityEventFloat onChangeEvent;
        public List<ValueCheckEvent> compareEvents;
    }
    [SerializeField] Events events;

    [System.Serializable]
    public class ValueCheckEvent
    {
        public ENumberCompareMode eCompareMode;
        public FloatReference testValue;
        public UnityEventFloat OnTrue;
    }

    private void OnEnable()
    {
        value.Value = defaultValue.Value;
    }

    void OnChange()
    {
        events.onChangeEvent.Invoke(value.Value);
        if (events.compareEvents.Count > 0)
        {
            foreach (ValueCheckEvent valueCheckEvent in events.compareEvents)
            {
                switch (valueCheckEvent.eCompareMode)
                {
                    case ENumberCompareMode.Equals:
                        if (value.Value == valueCheckEvent.testValue.Value)
                            valueCheckEvent.OnTrue.Invoke(value.Value);
                        break;
                    case ENumberCompareMode.GreaterThan:
                        if (value.Value > valueCheckEvent.testValue.Value)
                            valueCheckEvent.OnTrue.Invoke(value.Value);
                        break;
                    case ENumberCompareMode.LessThan:
                        if (value.Value < valueCheckEvent.testValue.Value)
                            valueCheckEvent.OnTrue.Invoke(value.Value);
                        break;
                    case ENumberCompareMode.LessThanOrEqual:
                        if (value.Value <= valueCheckEvent.testValue.Value)
                            valueCheckEvent.OnTrue.Invoke(value.Value);
                        break;
                    case ENumberCompareMode.GreaterThanOrEqual:
                        if (value.Value >= valueCheckEvent.testValue.Value)
                            valueCheckEvent.OnTrue.Invoke(value.Value);
                        break;
                }
            }
        }
    }

    public void Add(float _value)
    {
        Value = Value + _value;
    }
    public void Increase()
    {
        Value = Value + 1;
    }
    public void Decrease()
    {
        Value = Value - 1;
    }
}
