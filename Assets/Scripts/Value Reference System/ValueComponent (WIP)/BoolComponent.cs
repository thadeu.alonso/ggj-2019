﻿using UnityEngine;

public class BoolComponent : MonoBehaviour
{
    public string name;
    [SerializeField] BoolReference value; public bool Value { get { return this.value.Value; } set { if (this.value.Value == value) return; this.value.Value = value; onChangeEvent.Invoke(value); } }
    [SerializeField] BoolReference defaultValue;
    [SerializeField] protected UnityEventBool onChangeEvent;

    private void OnEnable()
    {
        value.Value = defaultValue.Value;
    }
}
