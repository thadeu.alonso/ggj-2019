﻿using UnityEngine;

[System.Serializable]
public class TransformReference : ValueReference
{
    [SerializeField] Transform constantValue;
    [SerializeField] TransformSO SOValue;
    [SerializeField] TransformComponent componentValue;

    public Transform Value
    {
        get
        {
            switch (valueType)
            {
                case 0:
                    return constantValue;
                case 1:
                    return SOValue == null ? null : SOValue.Value;
                case 2:
                    return componentValue == null ? null : componentValue.Value;
                default:
                    return null;
            }
        }
        set
        {
            switch (valueType)
            {
                case 0:
                    constantValue = value;
                    break;
                case 1:
                    if (SOValue != null)
                    {
                        TransformVariableSO var = (TransformVariableSO)SOValue;
                        if (var != null)
                            var.SetValue(value);
                    }
                    break;
                case 2:
                    if (componentValue != null)
                        componentValue.Value = value;
                    break;
            }
        }
    }
}
