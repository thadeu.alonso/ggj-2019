﻿using UnityEngine;

[System.Serializable]
public class FloatReference : ValueReference
{
    [SerializeField] float constantValue;
    [SerializeField] FloatSO SOValue;
    [SerializeField] FloatComponent componentValue;

    public float Value
    {
        get
        {
            switch (valueType)
            {
                case 0:
                    return constantValue;
                case 1:
                    return SOValue == null ? 0 : SOValue.Value;
                case 2:
                    return componentValue == null ? 0 : componentValue.Value;
                default:
                    return 0;
            }
        }
        set
        {
            switch (valueType)
            {
                case 0:
                    constantValue = value;
                    break;
                case 1:
                    if (SOValue != null)
                    {
                        FloatVariableSO SOVariable = (FloatVariableSO)SOValue;
                        if (SOVariable != null)
                            SOVariable.SetValue(value);
                    }
                    break;
                case 2:
                    if (componentValue != null)
                        componentValue.Value = value;
                    break;
            }
        }
    }
}
