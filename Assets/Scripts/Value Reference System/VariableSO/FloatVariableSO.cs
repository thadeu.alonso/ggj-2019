﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewFloatVariable", menuName = "Values/Variables/Float")]
public class FloatVariableSO : FloatSO, IVariable
{
    [SerializeField] protected float onPlayValue;
    [SerializeField] protected UnityEventFloat onChangeEvent;
    private List<FloatVariableListener> listeners = new List<FloatVariableListener>();

    private void OnEnable()
    {
        // NAO invokar onChangeEvent no OnEnable! Parece que ele perde os metodos atrelados via inspector
        _value = onPlayValue;
    }
    public void SetValue(float _newValue)
    {
        if (_value == _newValue)
            return;
        _value = _newValue;
        RaiseOnChangeEvent();
    }
    public void SetValue(FloatSO _floatSO)
    {
        if (_floatSO != null)
            SetValue(_floatSO.Value);
    }
    public void SetValue(FloatComponent _floatComponent)
    {
        if (_floatComponent != null)
            SetValue(_floatComponent.Value);
    }

    protected void RaiseOnChangeEvent()
    {
        onChangeEvent.Invoke(_value);
        for (int i = listeners.Count - 1; i >= 0; i--)
            listeners[i].OnChangeEventRaised(this);
    }

    public void Add(float _value)
    {
        SetValue(Value + _value);
    }
    public void Increase()
    {
        SetValue(Value + 1);
    }
    public void Decrease()
    {
        SetValue(Value - 1);
    }

    public void RegisterListener(FloatVariableListener listener)
    {
        listeners.Add(listener);
    }
    public void UnregisterListener(FloatVariableListener listener)
    {
        listeners.Remove(listener);
    }
}
