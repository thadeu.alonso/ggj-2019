﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class BoolVariableListener : MonoBehaviour
{
    [SerializeField] bool invokeOnStart;

    [System.Serializable]
    public class Listener
    {
        public BoolVariableSO[] boolVars;
        public UnityEventBool onChangeEvent;
        public List<ValueCheckEvent> compareEvents;
    }
    [SerializeField] List<Listener> listeners;

    [System.Serializable]
    public class ValueCheckEvent
    {
        public BoolReference compareValue;
        [System.Serializable]
        public class Events
        {
            public UnityEvent onTrue;
            public UnityEvent onFalse;
        }
        public Events response;
        public void Check(bool _value)
        {
            if (_value == compareValue.Value)
                response.onTrue.Invoke();
            else
                response.onFalse.Invoke();
        }
    }

    private void OnEnable()
    {
        foreach (var listener in listeners)
            foreach (var boolVar in listener.boolVars)
                if (boolVar != null)
                    boolVar.RegisterListener(this);
    }

    private void OnDisable()
    {
        foreach (var listener in listeners)
            foreach (var boolVar in listener.boolVars)
                if (boolVar != null)
                    boolVar.UnregisterListener(this);
    }

    private void Start()
    {
        if (invokeOnStart)
            foreach (var listener in listeners)
                foreach (var boolVar in listener.boolVars)
                    OnChangeEventRaised(boolVar);
    }

    public void OnChangeEventRaised(BoolVariableSO _boolVar)
    {
        if (_boolVar == null)
            return;

        Listener listener = listeners.Find(x => x.boolVars.ToList().Find(y => y == _boolVar));

        if (listener == null)
            return;

        listener.onChangeEvent.Invoke(_boolVar.Value);

        if (listener.compareEvents != null && listener.compareEvents.Count > 0)
        {
            foreach (ValueCheckEvent valueCheckEvent in listener.compareEvents)
                valueCheckEvent.Check(_boolVar.Value);
        }
    }
}