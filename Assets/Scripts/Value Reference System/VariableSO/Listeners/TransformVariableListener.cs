﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TransformVariableListener : MonoBehaviour
{
    [SerializeField] bool invokeOnStart;

    [System.Serializable]
    public class Listener
    {
        public TransformVariableSO[] transformVars;
        public UnityEventTransform onChangeEvent;
    }
    [SerializeField] List<Listener> listeners;

    private void OnEnable()
    {
        foreach (var listener in listeners)
            foreach (var transformVar in listener.transformVars)
                if (transformVar != null)
                    transformVar.RegisterListener(this);
    }

    private void OnDisable()
    {
        foreach (var listener in listeners)
            foreach (var transformVar in listener.transformVars)
                if (transformVar != null)
                    transformVar.UnregisterListener(this);
    }

    private void Start()
    {
        if (invokeOnStart)
            foreach (var listener in listeners)
                foreach (var transformVar in listener.transformVars)
                    OnChangeEventRaised(transformVar);
    }

    public void OnChangeEventRaised(TransformVariableSO _transformVar)
    {
        if (_transformVar == null)
            return;

        Listener listener = listeners.Find(x => x.transformVars.ToList().Find(y => y == _transformVar));

        if (listener != null)
            listener.onChangeEvent.Invoke(_transformVar.Value);
    }
}