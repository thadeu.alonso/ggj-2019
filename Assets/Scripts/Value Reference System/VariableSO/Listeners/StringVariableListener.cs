﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StringVariableListener : MonoBehaviour
{
    [SerializeField] bool invokeOnStart;

    [System.Serializable]
    public class Listener
    {
        public StringVariableSO[] stringVars;
        public UnityEventString OnChangeEvent;
    }
    [SerializeField] List<Listener> listeners;

    private void OnEnable()
    {
        foreach (var listener in listeners)
            foreach (var stringVar in listener.stringVars)
                if (stringVar != null)
                    stringVar.RegisterListener(this);
    }

    private void OnDisable()
    {
        foreach (var listener in listeners)
            foreach (var stringVar in listener.stringVars)
                if (stringVar != null)
                    stringVar.UnregisterListener(this);
    }

    private void Start()
    {
        if (invokeOnStart)
            foreach (var listener in listeners)
                foreach (var stringVar in listener.stringVars)
                    OnChangeEventRaised(stringVar);
    }

    public void OnChangeEventRaised(StringVariableSO _stringVar)
    {
        if (_stringVar == null)
            return;

        Listener listener = listeners.Find(x => x.stringVars.ToList().Find(y => y == _stringVar));

        if (listener != null)
            listener.OnChangeEvent.Invoke(_stringVar.Value);
    }
}