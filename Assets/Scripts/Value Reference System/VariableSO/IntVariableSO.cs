﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewIntVariable", menuName = "Values/Variables/Int")]
public class IntVariableSO : IntSO, IVariable
{
    [SerializeField] protected int onPlayValue;
    [SerializeField] protected UnityEventInt onChangeEvent;
    private List<IntVariableListener> listeners = new List<IntVariableListener>();

    private void OnEnable()
    {
        // NAO invokar onChangeEvent no OnEnable! Parece que ele perde os metodos atrelados via inspector
        _value = onPlayValue;
    }
    public void SetValue(int _newValue)
    {
        if (_value == _newValue)
            return;
        _value = _newValue;
        RaiseOnChangeEvent();
    }
    public void SetValue(IntSO _intSO)
    {
        if (_intSO != null)
            SetValue(_intSO.Value);
    }
    public void SetValue(IntComponent _intComponent)
    {
        if (_intComponent != null)
            SetValue(_intComponent.Value);
    }

    protected void RaiseOnChangeEvent()
    {
        onChangeEvent.Invoke(_value);
        for (int i = listeners.Count - 1; i >= 0; i--)
            listeners[i].OnChangeEventRaised(this);
    }

    public void Add(int _value)
    {
        SetValue(Value + _value);
    }
    public void Increase()
    {
        SetValue(Value + 1);
    }
    public void Decrease()
    {
        SetValue(Value - 1);
    }

    public void RegisterListener(IntVariableListener listener)
    {
        listeners.Add(listener);
    }
    public void UnregisterListener(IntVariableListener listener)
    {
        listeners.Remove(listener);
    }
}
