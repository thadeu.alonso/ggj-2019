﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewFloatConstant", menuName = "Values/Constants/Float")]
public class FloatSO : ScriptableObject, IConstant
{
    [SerializeField] protected float _value;
    public float Value { get { return _value; } }
}
