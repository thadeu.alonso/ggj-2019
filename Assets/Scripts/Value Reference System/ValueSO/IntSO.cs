﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewIntConstant", menuName = "Values/Constants/Int")]
public class IntSO : ScriptableObject, IConstant
{
    [SerializeField] protected int _value;
    public int Value { get { return _value; } }
}
