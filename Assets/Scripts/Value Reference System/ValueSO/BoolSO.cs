﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewBoolConstant", menuName = "Values/Constants/Bool")]
public class BoolSO : ScriptableObject, IConstant
{
    [SerializeField] protected bool _value;
    public bool Value { get { return _value; } }
}

public interface IConstant { }