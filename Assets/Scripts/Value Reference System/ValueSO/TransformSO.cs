﻿using UnityEngine;

public abstract class TransformSO : ScriptableObject
{
    [SerializeField] protected Transform _value;
    public Transform Value { get { return _value; } }
}
