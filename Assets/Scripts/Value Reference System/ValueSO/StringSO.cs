﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewStringConstant", menuName = "Values/Constants/String")]
public class StringSO : ScriptableObject, IConstant
{
    [SerializeField] protected string _value;
    public string Value { get { return _value; } }
}
