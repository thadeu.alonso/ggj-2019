﻿using UnityEngine;

[System.Serializable]
public class BoolReference : ValueReference
{
    [SerializeField] bool constantValue;
    [SerializeField] BoolSO SOValue;
    [SerializeField] BoolComponent componentValue; 

    public bool Value
    {
        get
        {
            switch (valueType)
            {
                case 0:
                    return constantValue;
                case 1:
                    return SOValue == null ? false : SOValue.Value;
                case 2:
                    return componentValue == null ? false : componentValue.Value;
                default:
                    return false;
            }
        }
        set
        {
            switch (valueType)
            {
                case 0:
                    constantValue = value;
                    break;
                case 1:
                    if (SOValue != null)
                    {
                        BoolVariableSO SOVariable = (BoolVariableSO) SOValue;
                        if (SOVariable != null)
                            SOVariable.SetValue(value);
                    }
                    break;
                case 2:
                    if (componentValue != null)
                        componentValue.Value = value;
                    break;
            }
        }
    }
}
