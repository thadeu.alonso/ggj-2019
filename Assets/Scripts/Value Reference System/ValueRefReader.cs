﻿using UnityEngine;

public class ValueRefReader : MonoBehaviour
{
    public bool invokeOnStart;

    [System.Serializable]
    public class FloatRead
    {
        public FloatReference[] floatVars;
        public UnityEventFloat onRead;
    }
    [SerializeField] FloatRead[] readFloat;

    [System.Serializable]
    public class IntRead
    {
        public IntReference[] intVars;
        public UnityEventInt onRead;
    }
    [SerializeField] IntRead[] readInt;

    [System.Serializable]
    public class BoolRead
    {
        public BoolReference[] boolVars;
        public UnityEventBool onRead;
    }
    [SerializeField] BoolRead[] readBool;

    [System.Serializable]
    public class TransformRead
    {
        public TransformReference[] transformVars;
        public UnityEventTransform onRead;
    }
    [SerializeField] TransformRead[] readTransform;

    [System.Serializable]
    public class StringRead
    {
        public StringReference[] var;
        public UnityEventString onRead;
    }
    [SerializeField] StringRead[] readString;

    private void Start()
    {
        if (invokeOnStart)
            ReadAll();
    }

    public void ReadAll()
    {
        ReadBool();
        ReadInt();
        ReadFloat();
        ReadString();
        ReadTransform();
    }

    public void ReadBool()
    {
        foreach (var read in readBool)
            foreach (var _var in read.boolVars)
                if (_var != null)
                    read.onRead.Invoke(_var.Value);
    }
    public void ReadInt()
    {
        foreach (var read in readInt)
            foreach (var _var in read.intVars)
                if (_var != null)
                    read.onRead.Invoke(_var.Value);
    }
    public void ReadFloat()
    {
        foreach (var read in readFloat)
            foreach (var _var in read.floatVars)
                if (_var != null)
                    read.onRead.Invoke(_var.Value);
    }
    public void ReadString()
    {
        foreach (var read in readString)
            foreach (var _var in read.var)
                if (_var != null)
                    read.onRead.Invoke(_var.Value);
    }
    public void ReadTransform()
    {
        foreach (var read in readTransform)
            foreach (var _var in read.transformVars)
                if (_var != null)
                    read.onRead.Invoke(_var.Value);
    }
}
