﻿using UnityEngine;
using UnityEngine.Events;

public class DelayedEvent : MonoBehaviour
{
    [SerializeField] FloatReference delay;
    public float Delay
    {
        get
        {
            return delay.Value;
        }

        set
        {
            if (delay.Value == value)
                return;

            float oldDelay = delay.Value;
            delay.Value = Mathf.Max(0, value);
            if (counting)
                currentTime += value - oldDelay;
        }
    }
    [SerializeField] bool onStart;
    [SerializeField] bool repeat;
    [SerializeField] bool unscaledDeltaTime;
    public bool Repeat
    {
        get
        {
            return repeat;
        }
        set
        {
            if (repeat == value)
                return;

            repeat = value;

            if (repeat == false && isRepeating)
                StopCounting();
        }
    }
    [SerializeField] UnityEvent response;
    
    float currentTime;
    bool counting;
    bool isRepeating;

    private void Awake()
    {
        currentTime = delay.Value;
    }

    private void Start()
    {
        if (onStart)
            StartTimer();
    }

    private void Update()
    {
        if (!counting)
            return;

        if (currentTime > 0)
        {
            if(!unscaledDeltaTime)
                currentTime -= Time.deltaTime;
            else
                currentTime -= Time.unscaledDeltaTime;

            if (currentTime <= 0)
                Raise();
        }
        else
            Raise();
    }

    public void StartTimer()
    {
        counting = true;
        currentTime = delay.Value;
        if (currentTime <= 0)
            Raise();
    }

    void Raise()
    {
        response.Invoke();
        if (repeat)
        {
            isRepeating = true;
            currentTime = delay.Value;
        }
        else
            StopCounting();
    }

    void StopCounting()
    {
        counting = false;
    }
}
