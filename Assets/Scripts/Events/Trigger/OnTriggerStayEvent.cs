﻿using UnityEngine;

public class OnTriggerStayEvent : OnTriggerEvent
{
    private void OnTriggerStay(Collider other)
    {
        Raise(other);
    }
}