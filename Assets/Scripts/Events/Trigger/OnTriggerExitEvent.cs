﻿using UnityEngine;

public class OnTriggerExitEvent : OnTriggerEvent
{
    private void OnTriggerExit(Collider other)
    {
        Raise(other);
    }
}