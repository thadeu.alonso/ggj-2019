﻿using UnityEngine;

public class OnTriggerEnterEvent : OnTriggerEvent
{
    private void OnTriggerEnter(Collider other)
    {
        Raise(other);
    }
}