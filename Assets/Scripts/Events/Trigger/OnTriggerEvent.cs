﻿using UnityEngine;

enum EIsTriggerMask { all, onlyIsTrigger, ignoreIsTrigger }

public abstract class OnTriggerEvent : MonoBehaviour
{
    [SerializeField] EIsTriggerMask isTriggerMask;
    [SerializeField] UnityEventTransform response;

    protected void Raise(Collider other)
    {
        switch (isTriggerMask)
        {
            case EIsTriggerMask.all:
                break;
            case EIsTriggerMask.onlyIsTrigger:
                if (!other.isTrigger)
                    return;
                break;
            case EIsTriggerMask.ignoreIsTrigger:
                if (other.isTrigger)
                    return;
                break;
        }
        response.Invoke(other.transform);
    }
}
