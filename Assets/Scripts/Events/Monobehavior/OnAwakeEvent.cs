﻿using UnityEngine;
using UnityEngine.Events;

public class OnAwakeEvent : MonoBehaviour
{
    [SerializeField] UnityEvent onAwake;
 
	void Awake ()
    {
        onAwake.Invoke();
    }	
}
