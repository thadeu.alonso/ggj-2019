﻿using UnityEngine;
using UnityEngine.Events;

public class OnUpdateEvent : MonoBehaviour
{
    [SerializeField] UnityEvent onUpdate;

    private void Update()
    {
        onUpdate.Invoke();
    }
}