﻿using UnityEngine;
using UnityEngine.Events;

public class OnDisableEvent : MonoBehaviour
{
    [SerializeField] UnityEvent onDisable;
 
	void OnDisable ()
    {
        onDisable.Invoke();
    }	
}
