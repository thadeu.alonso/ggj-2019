﻿using UnityEngine;
using UnityEngine.Events;

public class OnStartEvent : MonoBehaviour
{
    [SerializeField] UnityEvent onStart;
 
	void Start ()
    {
        onStart.Invoke();
    }	
}
