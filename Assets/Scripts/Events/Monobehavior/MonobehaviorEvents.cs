﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MonobehaviorEvents : MonoBehaviour
{
    [SerializeField] bool onAwake; public bool OnAwake { get { return onAwake; } set { onAwake = value; } }
    [SerializeField] bool _onEnable; public bool onEnable { get { return _onEnable; } set { _onEnable = value; } }
    [SerializeField] bool onStart; public bool OnStart { get { return onStart; } set { onStart = value; } }
    [SerializeField] bool onUpdate; public bool OnUpdate { get { return onUpdate; } set { onUpdate = value; } }

    [System.Serializable]
    public class Events
    {
        public UnityEvent OnAwake;
        public UnityEvent OnEnable;
        public UnityEvent OnStart;
        public UnityEvent OnUpdate;
    }
    [SerializeField] Events events;
    
    private void OnEnable()
    {
        if(onEnable)
            events.OnEnable.Invoke();
    }

    private void Awake()
    {
        if (onAwake)
            events.OnAwake.Invoke();
    }

    private void Start()
    {
        if (onStart)
            events.OnStart.Invoke();
    }

    private void Update()
    {
        if (onUpdate)
            events.OnUpdate.Invoke();
    }
}
