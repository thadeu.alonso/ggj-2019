﻿using UnityEngine;
using UnityEngine.Events;

public enum EConditionType { AND, OR }

public class ConditionalEvent : MonoBehaviour
{
    [SerializeField] bool onStart;
    [System.Serializable]
    public class Events
    {
        public EConditionType conditionType;
        [System.Serializable]
        public class Condition
        {
            [System.Serializable]
            public class BoolCondition
            {
                public BoolReference varTest;
                public BoolReference compareValue; }
            public BoolCondition[] boolConditions;

            [System.Serializable]
            public class FloatCondition
            {
                public FloatReference varTest;
                public ENumberCompareMode eCompareMode;
                public FloatReference compareValue; }
            public FloatCondition[] floatConditions;
        }
        public Condition conditions;

        [System.Serializable]
        public class Responses
        {
            public UnityEvent isTrue;
            public UnityEvent isFalse;
        }
        public Responses responses;

        public void Check()
        {
            if (conditionType == EConditionType.AND)
            {
                foreach (var boolCondition in conditions.boolConditions)
                {
                    if (boolCondition.varTest != boolCondition.compareValue)
                    {
                        responses.isFalse.Invoke();
                        return;
                    }
                }
                foreach (var floatCondition in conditions.floatConditions)
                {
                    switch (floatCondition.eCompareMode)
                    {
                        case ENumberCompareMode.Equals:
                            if (floatCondition.varTest.Value != floatCondition.compareValue.Value)
                            {
                                responses.isFalse.Invoke();
                                return;
                            }
                            break;
                        case ENumberCompareMode.GreaterThan:
                            if (floatCondition.varTest.Value <= floatCondition.compareValue.Value)
                            {
                                responses.isFalse.Invoke();
                                return;
                            }
                            break;
                        case ENumberCompareMode.LessThan:
                            if (floatCondition.varTest.Value >= floatCondition.compareValue.Value)
                            {
                                responses.isFalse.Invoke();
                                return;
                            }
                            break;
                        case ENumberCompareMode.LessThanOrEqual:
                            if (floatCondition.varTest.Value > floatCondition.compareValue.Value)
                            {
                                responses.isFalse.Invoke();
                                return;
                            }
                            break;
                        case ENumberCompareMode.GreaterThanOrEqual:
                            if (floatCondition.varTest.Value < floatCondition.compareValue.Value)
                            {
                                responses.isFalse.Invoke();
                                return;
                            }
                            break;
                    }
                }
                responses.isTrue.Invoke();
            }

            if (conditionType == EConditionType.OR)
            {
                foreach (var boolCondition in conditions.boolConditions)
                {
                    if (boolCondition.varTest == boolCondition.compareValue)
                    {
                        responses.isTrue.Invoke();
                        return;
                    }
                }
                foreach (var floatCondition in conditions.floatConditions)
                {
                    switch (floatCondition.eCompareMode)
                    {
                        case ENumberCompareMode.Equals:
                            if (floatCondition.varTest.Value == floatCondition.compareValue.Value)
                            {
                                responses.isTrue.Invoke();
                                return;
                            }
                            break;
                        case ENumberCompareMode.GreaterThan:
                            if (floatCondition.varTest.Value > floatCondition.compareValue.Value)
                            {
                                responses.isTrue.Invoke();
                                return;
                            }
                            break;
                        case ENumberCompareMode.LessThan:
                            if (floatCondition.varTest.Value < floatCondition.compareValue.Value)
                            {
                                responses.isTrue.Invoke();
                                return;
                            }
                            break;
                        case ENumberCompareMode.LessThanOrEqual:
                            if (floatCondition.varTest.Value <= floatCondition.compareValue.Value)
                            {
                                responses.isTrue.Invoke();
                                return;
                            }
                            break;
                        case ENumberCompareMode.GreaterThanOrEqual:
                            if (floatCondition.varTest.Value >= floatCondition.compareValue.Value)
                            {
                                responses.isTrue.Invoke();
                                return;
                            }
                            break;
                    }
                }
                responses.isFalse.Invoke();
            }
        }
    }
    public Events[] events;

    private void Start()
    {
        if (onStart)
            Raise();
    }

    public void Raise()
    {
        foreach (var _event in events)
            _event.Check();
    }
}
