﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewFloatEvent", menuName = "Events/Float")]
public class FloatEventSO : EventSO
{
    [SerializeField] protected UnityEventFloat onRaise;
    protected List<FloatEventListener> listeners = new List<FloatEventListener>();

    public void Raise(float _value)
    {
        onRaise.Invoke(_value);
        for (int i = listeners.Count - 1; i >= 0; i--)
            listeners[i].OnEventRaised(this, _value);
    }
    public void Raise(FloatSO _floatSO)
    {
        if (_floatSO != null)
            Raise(_floatSO.Value);
    }
    public void Raise(FloatComponent _floatComponent)
    {
        if (_floatComponent != null)
            Raise(_floatComponent.Value);
    }

    public void RegisterListener(FloatEventListener listener)
    {
        listeners.Add(listener);
    }
    public void UnregisterListener(FloatEventListener listener)
    {
        listeners.Remove(listener);
    }
}