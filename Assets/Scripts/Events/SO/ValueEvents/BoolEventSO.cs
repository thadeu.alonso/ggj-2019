﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewBoolEvent", menuName = "Events/Bool")]
public class BoolEventSO : ScriptableObject
{
    [SerializeField] protected UnityEventBool onRaise;
    protected List<BoolEventListener> listeners = new List<BoolEventListener>();

    public void Raise(bool _value)
    {
        onRaise.Invoke(_value);
        for (int i = listeners.Count - 1; i >= 0; i--)
            listeners[i].OnEventRaised(this, _value);
    }
    public void Raise(BoolSO _boolSO)
    {
        if (_boolSO != null)
            Raise(_boolSO.Value);
    }
    public void Raise(BoolComponent _boolComponent)
    {
        if (_boolComponent != null)
            Raise(_boolComponent.Value);
    }

    public void RegisterListener(BoolEventListener listener)
    {
        listeners.Add(listener);
    }
    public void UnregisterListener(BoolEventListener listener)
    {
        listeners.Remove(listener);
    }
}