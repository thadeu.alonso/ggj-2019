﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewIntEvent", menuName = "Events/Int")]
public class IntEventSO : EventSO
{
    [SerializeField] private UnityEventInt onRaise;
    protected List<IntEventListener> listeners = new List<IntEventListener>();

    public void Raise(int _value)
    {
        onRaise.Invoke(_value);
        for (int i = listeners.Count - 1; i >= 0; i--)
            listeners[i].OnEventRaised(this, _value);
    }
    public void Raise(IntSO _intSO)
    {
        if (_intSO != null)
            Raise(_intSO.Value);
    }
    public void Raise(IntComponent _intComponent)
    {
        if (_intComponent != null)
            Raise(_intComponent.Value);
    }

    public void RegisterListener(IntEventListener listener)
    {
        listeners.Add(listener);
    }
    public void UnregisterListener(IntEventListener listener)
    {
        listeners.Remove(listener);
    }
}