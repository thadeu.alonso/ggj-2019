﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewTransformEvent", menuName = "Events/Transform")]
public class TransformEventSO : EventSO
{
    [SerializeField] protected UnityEventTransform onRaise;
    protected List<TransformEventListener> listeners = new List<TransformEventListener>();

    public void Raise(Transform _value)
    {
        onRaise.Invoke(_value);
        for (int i = listeners.Count - 1; i >= 0; i--)
            listeners[i].OnEventRaised(this, _value);
    }
    public void Raise(TransformSO _transformSO)
    {
        if (_transformSO != null)
            Raise(_transformSO.Value);
    }
    public void Raise(TransformComponent _transformComponent)
    {
        if (_transformComponent != null)
            Raise(_transformComponent.Value);
    }

    public void RegisterListener(TransformEventListener listener)
    {
        listeners.Add(listener);
    }
    public void UnregisterListener(TransformEventListener listener)
    {
        listeners.Remove(listener);
    }
}