﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewStringEvent", menuName = "Events/String")]
public class StringEventSO : EventSO
{
    [SerializeField] protected UnityEventString onRaise;
    protected List<StringEventListener> listeners = new List<StringEventListener>();

    public void Raise(string _value)
    {
        onRaise.Invoke(_value);
        for (int i = listeners.Count - 1; i >= 0; i--)
            listeners[i].OnEventRaised(this, _value);
    }
    public void Raise(StringSO _stringSO)
    {
        if (_stringSO != null)
            Raise(_stringSO.Value);
    }
    public void Raise(StringComponent _stringComponent)
    {
        if (_stringComponent != null)
            Raise(_stringComponent.Value);
    }

    public void RegisterListener(StringEventListener listener)
    {
        listeners.Add(listener);
    }
    public void UnregisterListener(StringEventListener listener)
    {
        listeners.Remove(listener);
    }
}