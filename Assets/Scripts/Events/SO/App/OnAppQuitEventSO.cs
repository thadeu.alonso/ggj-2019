﻿using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "ApplicationQuitEvent", menuName = "AppEvents/Application Quit")]
public class OnAppQuitEventSO : ScriptableObject
{
    [SerializeField] private UnityEvent onRaise;

    public void Init()
    {
        Application.quitting += onRaise.Invoke;
    }
}
