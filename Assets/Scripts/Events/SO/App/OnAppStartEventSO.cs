﻿using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "OnAppStartEvent", menuName = "AppEvents/OnAppStart")]
public class OnAppStartEventSO : ScriptableObject
{
    [SerializeField] private UnityEvent onRaise;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    public static void Init()
    {
        OnAppStartEventSO instance = FindThisAssetInstance();
        if (instance != null)
            instance.onRaise.Invoke();
        else
            Debug.Log("<color=red>! Evento de Application Start não criado! Ou  nao está na pasta Resources</color>");
    }

    static OnAppStartEventSO FindThisAssetInstance()
    {
        Resources.LoadAll<OnAppStartEventSO>("");
        return Resources.FindObjectsOfTypeAll<OnAppStartEventSO>().FirstOrDefault();
    }
}
