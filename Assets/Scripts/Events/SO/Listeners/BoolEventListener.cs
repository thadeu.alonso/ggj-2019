﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class BoolEventListener : MonoBehaviour
{
    [System.Serializable]
    public class Listener
    {
        public BoolEventSO[] events;
        public UnityEventBool response;
    }
    public List<Listener> listeners;

    private void OnEnable()
    {
        foreach (var listener in listeners)
            foreach (var _event in listener.events)
                if (_event != null)
                    _event.RegisterListener(this);
    }

    private void OnDisable()
    {
        foreach (var listener in listeners)
            foreach (var _event in listener.events)
                if (_event != null)
                    _event.UnregisterListener(this);
    }

    public void OnEventRaised(BoolEventSO _event, bool _value)
    {
        if (_event == null)
            return;

        Listener listener = listeners.Find(x => x.events.ToList().Find(y => y == _event));

        if (listener != null)
            listener.response.Invoke(_value);
    }
}