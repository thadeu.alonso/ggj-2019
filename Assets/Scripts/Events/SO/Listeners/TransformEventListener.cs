﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class TransformEventListener : MonoBehaviour
{
    [System.Serializable]
    public class Listener
    {
        public TransformEventSO[] events;
        public UnityEventTransform response;
    }
    public List<Listener> listeners;

    private void OnEnable()
    {
        foreach (var listener in listeners)
            foreach (var _event in listener.events)
                if (_event != null)
                    _event.RegisterListener(this);
    }

    private void OnDisable()
    {
        foreach (var listener in listeners)
            foreach (var _event in listener.events)
                if (_event != null)
                    _event.UnregisterListener(this);
    }

    public void OnEventRaised(TransformEventSO _event, Transform _value)
    {
        if (_event == null)
            return;

        Listener listener = listeners.Find(x => x.events.ToList().Find(y => y == _event));

        if (listener != null)
            listener.response.Invoke(_value);
    }
}