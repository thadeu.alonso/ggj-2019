﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class VoidEventListener : MonoBehaviour
{
    [System.Serializable]
    public class Listener
    {
        public VoidEventSO[] events;
        public UnityEvent response;
    }
    public List<Listener> listeners;

    private void OnEnable()
    {
        foreach (var listener in listeners)
            foreach (var _event in listener.events)
                if (_event != null)
                    _event.RegisterListener(this);
    }

    private void OnDisable()
    {
        foreach (var listener in listeners)
            foreach (var _event in listener.events)
                if (_event != null)
                    _event.UnregisterListener(this);
    }

    public void OnEventRaised(VoidEventSO _event)
    {
        if (_event == null)
            return;

        List<Listener> listenersFond = listeners.FindAll(x => x.events.ToList().Find(y => y == _event));

        foreach (Listener listener in listenersFond)
            listener.response.Invoke();
    }
}
