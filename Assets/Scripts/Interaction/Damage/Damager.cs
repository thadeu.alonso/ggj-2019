﻿    using UnityEngine;

public class Damager : MonoBehaviour
{
    [SerializeField] IntReference damage;
    [System.Serializable]
    public class Events
    {
        public UnityEventTransform onDamage;
    }
    [SerializeField] Events events;

    public void TryDamage(Transform _other)
    {
        IDamageble damageble = _other.GetComponent<IDamageble>();
        if (damageble != null)
        {
            Damage(damageble);
            events.onDamage.Invoke(_other);
        }
    }

    public void Damage(IDamageble damageble)
    {
        damageble.ReceiveDamage(damage.Value);
    }
}
