﻿using UnityEngine;
using UnityEngine.Events;

public class IDamageble : MonoBehaviour
{
    [SerializeField] IntReference maxHealth;
    [SerializeField] BoolReference immortal;
    [System.Serializable]
    public class Events
    {
        public UnityEventInt onReceiveDamage;
        public UnityEvent onDie;
    }
    [SerializeField] Events events;

    int currentHealth;
    bool isDead;

    private void Awake()
    {
        currentHealth = maxHealth.Value;
    }

    public void ReceiveDamage(int _damage)
    {
        if (isDead)
            return;
        currentHealth = Mathf.Max(0, currentHealth -_damage);
        events.onReceiveDamage.Invoke(_damage);
        if (currentHealth == 0 && !immortal.Value)
            Die();
    }

    void Die()
    {
        isDead = true;
        events.onDie.Invoke();
    }
}
