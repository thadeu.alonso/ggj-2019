﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewAudio", menuName = "Audio/Audio")]
public class AudioSO : ScriptableObject
{
    [SerializeField] AudioClip clip; public AudioClip Clip { get { return clip; } }
    [Range(0, 1)]
    [SerializeField] float volume = 1; public float Volume { get { return volume; } }
}
