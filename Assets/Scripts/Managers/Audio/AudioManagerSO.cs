﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "AudioManager", menuName = "Managers/Audio Manager")]
public class AudioManagerSO : ManagerSO
{
    [SerializeField] BoolSO musicMuted;
    public bool MusicMuted
    {
        set
        {
            if (audioSource == null)
                CreateAudioSource();
            if (audioSource == null)
                return;

            audioSource.mute = value;
        }
    }
    [SerializeField] FloatSO musicVolume;
    [SerializeField] BoolSO sfxMuted;
    [SerializeField] FloatSO sfxVolume;

    CanvasGroupController coroutineHolder;
    AudioSource audioSource;
    float musicAudioSOVolume = 1;
    float globalMusicVolume = 1;

    private void OnEnable()
    {
        musicAudioSOVolume = 1;
        globalMusicVolume = 1;
    }

    public void PlaySfx(AudioSO _audioSO)
    {
        if (_audioSO == null || _audioSO.Clip == null 
            || (sfxMuted != null && sfxMuted.Value == true))
        {
            Debug.Log("Can NOT Play sfx");
            return;
        }

        if (audioSource == null)
            CreateAudioSource();
        if (audioSource == null)
        {
            Debug.Log("Can NOT create audio source");
            return;
        }

        float sfxGlobalVolume = 1;
        if (sfxVolume != null)
            sfxGlobalVolume = sfxVolume.Value;

        if (audioSource != null)
            audioSource.PlayOneShot(_audioSO.Clip, _audioSO.Volume * sfxGlobalVolume * globalMusicVolume);
    }
    public void PlayMusic(AudioSO _audioSO)
    {
        if (_audioSO == null || _audioSO.Clip == null
            || (musicMuted != null && musicMuted.Value == true))
        {
            Debug.Log("Can NOT Play music");
            return;
        }

        if (audioSource == null)
            CreateAudioSource();
        if (audioSource == null)
        {
            Debug.Log("Can NOT create audio source");
            return;
        }

        musicAudioSOVolume = _audioSO.Volume;

        float musicGlobalVolume = 1;
        if (musicVolume != null)
            musicGlobalVolume = musicVolume.Value;

        audioSource.loop = true;
        audioSource.clip = _audioSO.Clip;
        UpdateAudioSourceVolume();
        audioSource.Play();
    }

    public void SetGlobalVolumeSlowlyTo0DuringTime(float _time)
    {
        if (coroutineHolder == null)
            CreateAudioSource();
        if (coroutineHolder == null)
            return;

        coroutineHolder.StartCoroutine(COSetGlobalVolumeSlowlyTo0DuringTime(_time));
    }
    IEnumerator COSetGlobalVolumeSlowlyTo0DuringTime(float _time)
    {
        globalMusicVolume = 1;
        if (_time <= 0)
        {
            globalMusicVolume = 0;
            UpdateAudioSourceVolume();
            yield break;
        }
        float time = _time;
        while(time > 0)
        {
            yield return null;
            time -= Time.unscaledDeltaTime;
            globalMusicVolume = Mathf.Max(0, time / _time);
            UpdateAudioSourceVolume();
        }
    }

    public void SetGlobalVolumeSlowlyTo1DuringTime(float _time)
    {
        if (coroutineHolder == null)
            CreateAudioSource();
        if (coroutineHolder == null)
            return;

        coroutineHolder.StartCoroutine(COSetGlobalVolumeSlowlyTo1DuringTime(_time));
    }
    IEnumerator COSetGlobalVolumeSlowlyTo1DuringTime(float _time)
    {
        globalMusicVolume = 0;
        if (_time <= 0)
        {
            globalMusicVolume = 1;
            UpdateAudioSourceVolume();
            yield break;
        }
        float time = _time;
        while (time > 0)
        {
            yield return null;
            time -= Time.unscaledDeltaTime;
            globalMusicVolume = Mathf.Min(1, 1 - time / _time);
            UpdateAudioSourceVolume();
        }
    }

    void CreateAudioSource()
    {
        audioSource = new GameObject().AddComponent<AudioSource>();
        coroutineHolder = audioSource.gameObject.AddComponent<CanvasGroupController>();
        DontDestroyOnLoad(audioSource);
        audioSource.name = "AudioSource for AudioManager";
    }

    public void UpdateAudioSourceVolume()
    {
        if (audioSource == null)
            CreateAudioSource();
        if (audioSource == null)
            return;
        float musicVolumeValue = 1;
        if (musicVolume != null)
            musicVolumeValue = musicVolume.Value;
        audioSource.volume = Mathf.Min(1, Mathf.Max(0, musicVolumeValue * musicAudioSOVolume * globalMusicVolume));
    }
}
