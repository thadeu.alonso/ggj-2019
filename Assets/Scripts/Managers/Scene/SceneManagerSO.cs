﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum Escene { MenuInicial, LoadingScene, InGame0 }

[CreateAssetMenu(fileName = "SceneManager", menuName = "Managers/Scene Manager")]
public class SceneManagerSO : ManagerSO
{
    [SerializeField] Escene loadingScene;
    [SerializeField] GameObject screenFader;
    [SerializeField] float screenFadeDuration = 1.5f;
    [SerializeField] float startLoadingDelay = 2;
    [SerializeField] float minTimeOn50Percent = 1.5f;
    [SerializeField] float timeOn100Percent = 1;
    [SerializeField] float screenAppearDuration = 1f;
    [System.Serializable]
    public class Events {
        public UnityEvent startedLoadingScene;
        public UnityEventFloat loadingProgressUpdated;
        public UnityEvent endedLoadingScene; }
    [SerializeField] Events events;
    CanvasGroupController coroutineHolder;
    CanvasGroupController screenFaderInstance;
    bool isLoadingScene;

    [EnumAction(typeof(Escene))]
    public void LoadScene(int _newScene)
    {
        if (coroutineHolder == null)
            CreateCoroutineHolder();
        if (coroutineHolder == null)
        {
            Debug.Log("Could NOT create CoroutineHolder");
            return;
        }

        if (isLoadingScene)
        { 
            Debug.Log("Can NOT load scene. Already loading.");
            return;
        }
        if (!Application.CanStreamedLevelBeLoaded(((Escene)_newScene).ToString()))
        {
            Debug.Log("Could NOT find scene to load");
            return;
        }

        coroutineHolder.StartCoroutine(LoadSceneCo(coroutineHolder, (Escene)_newScene, screenFader));
    }

    void CreateCoroutineHolder()
    {
        isLoadingScene = false;
        coroutineHolder = new GameObject().AddComponent<CanvasGroupController>();
        DontDestroyOnLoad(coroutineHolder);
        coroutineHolder.name = "Coroutine holder for SceneManager";
    }

    IEnumerator LoadSceneCo(CanvasGroupController coroutineHolder, Escene _newScene, GameObject _screenFader)
    {
        events.startedLoadingScene.Invoke();
        isLoadingScene = true;
        Scene oldScene = SceneManager.GetActiveScene();
        string newSceneName = _newScene.ToString();

        yield return coroutineHolder.StartCoroutine(StartScreenFade(_screenFader));

        // Espera um tempo antes de carregar a loading scene (tela está escurecendo)
        yield return new WaitForSecondsRealtime(startLoadingDelay);

        float startTime = Time.time;

        // Carrega cena de Loading
        yield return coroutineHolder.StartCoroutine(LoadSceneAsync(loadingScene.ToString()));

        // Descarrega cena antiga
        yield return coroutineHolder.StartCoroutine(UnLoadSceneAsyncFirstHalfProgress(oldScene));

        events.loadingProgressUpdated.Invoke(0.5f);

        // Espera pelo tempo minimo em 50% caso nao tenha passado ainda
        float timePassed = Time.time - startTime;
        if (timePassed < minTimeOn50Percent)
            yield return new WaitForSecondsRealtime(minTimeOn50Percent - timePassed);

        // Salva objetos originais da cena de loading 
        Scene loadingScenne = SceneManager.GetSceneByName(loadingScene.ToString());
        GameObject[] loadingSceneObjects = loadingScenne.GetRootGameObjects();

        // Carrega nova cena
        yield return coroutineHolder.StartCoroutine(LoadSceneAsyncSecondHalfProgress(newSceneName));

        events.loadingProgressUpdated.Invoke(1);

        // Espera um tempo em 100% pra dar tempo do jogador ver o 100% por algum tempo
        yield return coroutineHolder.StartCoroutine(FreezeTimeDuringTime(timeOn100Percent));

        // Deleta objetos originais da cena de loading e junta qualquer objeto que tenha sido instanciado
        foreach (var _obj in loadingSceneObjects)
            Destroy(_obj);
        SceneManager.MergeScenes(loadingScenne, SceneManager.GetSceneByName(newSceneName));
        //SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName(loadingScene.ToString()));

        // Desescurecer tela e destruir controller e coroutine holder
        yield return coroutineHolder.StartCoroutine(StartScreenAppear());
        Destroy(coroutineHolder.gameObject);

        isLoadingScene = false;
        events.endedLoadingScene.Invoke();
    }

    IEnumerator StartScreenFade(GameObject _screenFader)
    {
        if (screenFader == null || screenFader.GetComponent<CanvasGroupController>() == null)
        {
            Debug.Log("Could not instantiate screenFaderPrefab");
        }
        else
        {
            screenFaderInstance = Instantiate(screenFader).GetComponent<CanvasGroupController>();
            screenFaderInstance.StartAppearing(screenFadeDuration);
            DontDestroyOnLoad(screenFaderInstance.gameObject);
        }
        yield return null;
    }
    IEnumerator LoadSceneAsync(string _name)
    {
        AsyncOperation asyncLoader = SceneManager.LoadSceneAsync(_name, LoadSceneMode.Additive);
        while (!asyncLoader.isDone)
        {
            yield return null;
        }
    }
    IEnumerator UnLoadSceneAsyncFirstHalfProgress(Scene _scene)
    {
        GameObject[] oldSceneObjects = _scene.GetRootGameObjects();

        int destroyedObjects = 0;
        int destroyedObjectsToWaitNextFrame = 5;
        for (int i = 0; i < oldSceneObjects.Length; i++)
        {
            Destroy(oldSceneObjects[i]);
            events.loadingProgressUpdated.Invoke(i * 1.0f / oldSceneObjects.Length / 2);
            destroyedObjects++;
            if(destroyedObjects >= destroyedObjectsToWaitNextFrame)
            {
                destroyedObjects = 0;
                yield return null;
            }
        }

        AsyncOperation asyncLoader = SceneManager.UnloadSceneAsync(_scene);
        while (!asyncLoader.isDone)
        {
            yield return null;
        }
    }
    IEnumerator LoadSceneAsyncSecondHalfProgress(string _name)
    {
        AsyncOperation asyncLoader = SceneManager.LoadSceneAsync(_name, LoadSceneMode.Additive);
        float progress = 0;
        while (!asyncLoader.isDone)
        {
            if (asyncLoader.progress > progress)
            {
                progress = asyncLoader.progress;
                events.loadingProgressUpdated.Invoke(0.5f + progress / 2.0f);
            }
            yield return null;
        }
    }
    IEnumerator FreezeTimeDuringTime(float _duration)
    {
        Time.timeScale = 0;
        yield return new WaitForSecondsRealtime(_duration);
        Time.timeScale = 1;
    }
    IEnumerator StartScreenAppear()
    {
        screenFaderInstance.StartDisappearing(screenAppearDuration);
        Destroy(screenFaderInstance.gameObject, screenAppearDuration);
        yield return null;
    }
}
